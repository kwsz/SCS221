def decode_base64(text):
  """
    Still getting errors with negitive padding im stuck
  """


  b64_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

  decoded = ""

  bits = 0

  for i in range(0, len(text), 4):

    # Get the next 4 base64 characters
    for j in range(4):
      if text[i + j] != "=":
        index = b64_table.find(text[i + j])
        bits |= index << (6 * (3 - j))

    # Convert 24 bits to 3 bytes of data
    for j in range(1, 4 - (text.count('=') - i)):
      decoded += chr((bits >> (8 * (3 - j))) & 0xFF)
  return decoded

def main():
  filename = input("Enter filename with base64 encoded text: ")
  with open(filename, "r") as f:
    encoded_text = f.read()
  decoded_text = decode_base64(encoded_text)
  print(decoded_text)

if __name__ == "__main__":
  main()