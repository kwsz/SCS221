def encode_base64(text):
  """
  Encodes text to base64 using a simple table and bit manipulation.

  Args:
    text: The text to be encoded.

  Returns:
    The base64 encoded string.
  """
  
  b64_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
  encoded = ""

  for i in range(0, len(text), 3):

    # Get the next 3 bytes of data
    bits = ord(text[i]) << 16
    if i + 1 < len(text):
      bits |= ord(text[i + 1]) << 8
    if i + 2 < len(text):
      bits |= ord(text[i + 2])

    # Convert 24 bits to 4 base64 characters
    for j in range(4):
      index = (bits >> (6 * (3 - j))) & 0x3F
      encoded += b64_table[index]

  # Add padding if needed
  if len(text) % 3 == 1:
    encoded += "=="
  elif len(text) % 3 == 2:
    encoded += "="
  return encoded

def main():
  filename = input("Enter filename to encode: ")
  with open(filename, "r") as f:
    text = f.read()
  encoded_text = encode_base64(text)
  print("Encoded text:")
  print(encoded_text)

if __name__ == "__main__":
  main()