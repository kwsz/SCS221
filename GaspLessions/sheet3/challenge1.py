from gasp import *

begin_graphics(width=500, height=500)
Circle((200, 250), 100)
Circle((160, 280), 10)
Circle((240, 280), 10)
Line((175, 210), (210, 250))
Line((175, 210), (220, 210))
Arc((200, 200), 30, 225, 315)

update_when('key_pressed')
end_graphics()
