from gasp import *      

player_x = 5
player_y = 5

begin_graphics() 
finished = False

def place_player():
    global player_shape
    player_shape = Circle((10 * player_x, 10 * player_y), 5, filled=True)
    print("Here I am!")

def move_player():
    global player_x, player_y, player_shape

    key = update_when('key_pressed')

    if key == 'd' and player_x < 63:
        player_x += 1
    elif key == 'w' and player_y < 63:
        player_y += 1
    elif key == 's' and player_y > 0:
        player_y += -1
    elif key == 'a' and player_x > 0:
        player_x += -1

    move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))
        

while not finished:
    place_player()
    move_player()

end_graphics() 