import time
from random import randint

st = time.time()

i = 0

for x in range(10):
    num1 = randint(0,10)

    num2 = randint(0,10)

    ans = input(f'What is {num1} * {num2}\n')

    if ans.isdigit() == True:
        if int(ans) == num1 * num2:
            print('That is the correct answer')
            i = i + 1
        else:
            print('Incorrect')
    else:
        print('Incorrect')

et = time.time()
elt = et - st

print(f'You got {i} amount of 10 correct in {elt} seconds')
