from random import *

random = randint(1,1000)
print(f"Your number has been generated")
print(random)
guess = int(input("Guess what number has been generated "))

if random > guess:
    closeness = random - guess
    print(f"You were {closeness} numbers away from the random number")

elif random < guess:
    closeness = guess - random
    print(f"You were {closeness} numbers away from the random number")

else:
    print("The numbers are the exact same")

