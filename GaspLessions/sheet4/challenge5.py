num = 1234
counter = 1

while True:
    guess = int(input("Enter a number "))
    if guess == 1234:
        print("You guessed the correct number")
        print(f"It took you {counter} attempts")
        break
    else:
        print("Try again")
        counter = counter + 1